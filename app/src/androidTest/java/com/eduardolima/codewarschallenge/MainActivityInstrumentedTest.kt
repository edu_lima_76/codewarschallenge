package com.eduardolima.codewarschallenge

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.eduardolima.codewarschallenge.activities.main.MainActivity
import com.eduardolima.codewarschallenge.pages.MainPage
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityInstrumentedTest {

    lateinit var mMainPage: MainPage

    @get:Rule
    val mActivityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setup() {
        mMainPage = MainPage()
    }

    @Test
    fun testMainActivity_isDisplayed() {
        mMainPage.isVisible()
    }

    @Test
    fun testMainActivity_searchForUser() {
        // act
        mMainPage.isVisible()
        mMainPage.fillUserName("g964")
        mMainPage.searchForUser()

        // assert
        mMainPage.verifyHaveSearchUser()
    }
}