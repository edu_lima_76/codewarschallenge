package com.eduardolima.codewarschallenge.pages

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.view.View
import com.eduardolima.codewarschallenge.R


class MainPage : PageObject() {

    fun isVisible() {
        onView(withId(R.id.searchContainer)).check(matches(isDisplayed()))
    }

    fun fillUserName(userId: String) {
        onView(withId(R.id.userEditText)).check(matches(isDisplayed()))
        onView(withId(R.id.userEditText)).perform(typeText(userId), closeSoftKeyboard())
    }

    fun searchForUser() {
        onView(withId(R.id.searchUser)).perform(click())
    }

    fun waitForProgess() {
        createIdlingResource(getActivityInstance().findViewById(R.id.mainProgress), "mainProgress", View.GONE, R.id.mainProgress)
    }

    fun verifyHaveSearchUser() {
        createIdlingResource(getActivityInstance().findViewById(R.id.listOfUsersRecyclerView), "listOfUsersRecyclerView", View.VISIBLE, R.id.listOfUsersRecyclerView)
        unregisterLatestIdlingResources()
    }
}