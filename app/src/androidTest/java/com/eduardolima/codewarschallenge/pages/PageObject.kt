package com.eduardolima.codewarschallenge.pages

import android.app.Activity
import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.IdlingResource
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import android.support.test.runner.lifecycle.Stage
import android.view.View
import com.eduardolima.codewarschallenge.GenericIdlingResource


open class PageObject {

    lateinit var idlingResource: IdlingResource

    fun createIdlingResource(view: View, tag: String, visibility: Int, viewId: Int) {
        idlingResource = GenericIdlingResource(view, tag, visibility, viewId)
        IdlingRegistry.getInstance().register(idlingResource)
    }

    fun unregisterLatestIdlingResources() {
        if (idlingResource != null) {
            IdlingRegistry.getInstance().unregister(idlingResource)
        }
    }

    internal fun getActivityInstance(): Activity {
        val currentActivity: MutableList<Activity> = arrayListOf()

        getInstrumentation().runOnMainSync({
            val resumedActivity = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED)
            val it = resumedActivity.iterator()
            currentActivity.add(it.next())
        })

        return currentActivity.get(0)
    }
}