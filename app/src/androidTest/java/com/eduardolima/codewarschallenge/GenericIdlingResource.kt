package com.eduardolima.codewarschallenge

import android.app.Activity
import android.support.test.espresso.IdlingResource
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import android.support.test.runner.lifecycle.Stage
import android.util.Log
import android.view.View


class GenericIdlingResource(private var mView: View?, private val mTag: String, private val mVisibility: Int, private val mViewId: Int) : IdlingResource {
    private var mResourceCallback: IdlingResource.ResourceCallback? = null

    internal val activityInstance: Activity
        get() {
            val currentActivity = arrayOf<Activity>()

            val resumedActivity = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED)
            val it = resumedActivity.iterator()
            currentActivity[0] = it.next()


            return currentActivity[0]
        }

    override fun getName(): String {
        return this.mTag
    }

    override fun isIdleNow(): Boolean {

        val idle = mView != null && mView!!.visibility == mVisibility


        Log.e(mTag, "idle: " + idle + " is view null? " + (mView == null))

        if (idle) {
            mResourceCallback!!.onTransitionToIdle()
        }
        if (mView == null) {
            mView = activityInstance.findViewById(mViewId)
        }
        return idle
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        this.mResourceCallback = callback
    }
}