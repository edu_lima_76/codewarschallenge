package com.eduardolima.codewarschallenge.activities.main

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(MainPresenterModule::class))
interface MainActivityComponent {
    fun inject(activity: MainActivity)
}