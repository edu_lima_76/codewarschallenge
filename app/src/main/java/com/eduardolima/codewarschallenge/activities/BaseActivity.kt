package com.eduardolima.codewarschallenge.activities

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.eduardolima.codewarschallenge.activities.codeChallenge.CodeChallengeActivity
import com.eduardolima.codewarschallenge.activities.userinfo.UserInfoActivity
import com.eduardolima.codewarschallenge.shared.components.ErrorDialogFragment
import com.eduardolima.codewarschallenge.shared.components.InfoDialogFragment
import com.eduardolima.codewarschallenge.shared.components.callback.ErrorDialogCallback
import com.eduardolima.codewarschallenge.shared.database.pojo.User


open class BaseActivity : AppCompatActivity() {

    protected val INTENT_USER_ID = "user_id"
    protected val INTENT_CHALLENGE_ID = "challenge_id"

    fun navigateToUserInfo(selectedUser: User) {
        val intent = Intent(this, UserInfoActivity::class.java)
        intent.putExtra(INTENT_USER_ID, selectedUser.username)
        startActivity(intent)
    }

    fun navigateToCodeChallenge(selectedChallengeId: String) {
        val intent = Intent(this, CodeChallengeActivity::class.java)
        intent.putExtra(INTENT_CHALLENGE_ID, selectedChallengeId)
        startActivity(intent)
    }

    fun showErrorDialog(dialogTitle: String,
                        dialogMessage: String,
                        callback: ErrorDialogCallback) {
        val fragmentManager = fragmentManager
        val errorDialogFragment = ErrorDialogFragment.newInstance(dialogTitle, dialogMessage, callback)
        errorDialogFragment.show(fragmentManager, "error_dialog_fragment")
    }

    fun showInfoDialog(dialogTitle: String,
                        dialogMessage: String) {
        val fragmentManager = fragmentManager
        val infoDialogFragment = InfoDialogFragment.newInstance(dialogTitle, dialogMessage)
        infoDialogFragment.show(fragmentManager, "error_dialog_fragment")
    }

    fun hideKeyboard(view: View) {
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}