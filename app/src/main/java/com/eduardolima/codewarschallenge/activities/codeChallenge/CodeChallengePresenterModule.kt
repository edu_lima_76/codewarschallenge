package com.eduardolima.codewarschallenge.activities.codeChallenge

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class CodeChallengePresenterModule(var mApp: Application) {
    @Provides
    @Singleton
    fun provideCodeChallengePresenter(): CodeChallengePresenter {
        return CodeChallengePresenter(mApp)
    }
}