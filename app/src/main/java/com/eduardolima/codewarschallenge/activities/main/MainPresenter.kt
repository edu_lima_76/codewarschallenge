package com.eduardolima.codewarschallenge.activities.main

import android.app.Application
import android.arch.lifecycle.LiveData
import com.eduardolima.codewarschallenge.shared.database.pojo.User
import com.eduardolima.codewarschallenge.shared.presenter.BasePresenter
import com.eduardolima.codewarschallenge.shared.services.Error
import com.eduardolima.codewarschallenge.shared.services.user.UserService
import com.eduardolima.codewarschallenge.shared.services.user.UserServiceModule
import com.eduardolima.codewarschallenge.shared.views.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import javax.inject.Inject


class MainPresenter(application: Application) : BasePresenter() {

    @Inject
    lateinit var mUserService: UserService

    init {
        DaggerMainPresenterComponent.builder()
                .userServiceModule(UserServiceModule(application))
                .build()
                .inject(this)
    }

    override fun processSuccess(action: Int) {
        if (mView != null) {
            mView!!.onActionSuccess(action)
        }
    }

    override fun processError(action: Int, @Error.Code errorCode: String?) {
        if (mView != null) {
            mView!!.onActionFail(action, errorCode)
        }
    }

    public override fun subscribe(@NonNull view: View) {
        super.subscribe(view)
        mUserService.getStream()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::processEvent)
    }

    public override fun unsubscribe() {
        super.unsubscribe()
    }

    /**
     * Get store list of users
     */
    fun getListOfUsers(): LiveData<List<User>> {
        return mUserService.getListOfUsers()
    }

    /**
     * Fetch user info
     */
    fun fetchUserInfo(userId: String) {
        mUserService.fetchUserInfo(userId)
    }
}