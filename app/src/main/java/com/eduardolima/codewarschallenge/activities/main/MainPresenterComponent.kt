package com.eduardolima.codewarschallenge.activities.main

import com.eduardolima.codewarschallenge.shared.services.user.UserServiceModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(UserServiceModule::class))
interface MainPresenterComponent {
    fun inject(presenter: MainPresenter)
}