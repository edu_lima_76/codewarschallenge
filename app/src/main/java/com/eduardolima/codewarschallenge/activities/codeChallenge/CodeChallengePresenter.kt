package com.eduardolima.codewarschallenge.activities.codeChallenge

import android.app.Application
import android.arch.lifecycle.LiveData
import com.eduardolima.codewarschallenge.shared.database.pojo.codechallenge.CodeChallenge
import com.eduardolima.codewarschallenge.shared.presenter.BasePresenter
import com.eduardolima.codewarschallenge.shared.services.Error
import com.eduardolima.codewarschallenge.shared.services.challenge.ChallengeService
import com.eduardolima.codewarschallenge.shared.services.challenge.ChallengeServiceModule
import com.eduardolima.codewarschallenge.shared.views.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import javax.inject.Inject


class CodeChallengePresenter(application: Application) : BasePresenter() {

    @Inject
    lateinit var mChallengeService: ChallengeService

    init {
        DaggerCodeChallengePresenterComponent.builder()
                .challengeServiceModule(ChallengeServiceModule(application))
                .build()
                .inject(this)
    }

    override fun processSuccess(action: Int) {
        if (mView != null) {
            mView!!.onActionSuccess(action)
        }
    }

    override fun processError(action: Int, @Error.Code errorCode: String?) {
        if (mView != null) {
            mView!!.onActionFail(action, errorCode)
        }
    }

    public override fun subscribe(@NonNull view: View) {
        super.subscribe(view)
        mChallengeService.getStream()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::processEvent)
    }

    public override fun unsubscribe() {
        super.unsubscribe()
    }

    /**
     * Get store info for the given challenge
     */
    fun getCodeChallenge(codeChallengeId: String): LiveData<CodeChallenge> {
        return mChallengeService.getCodeChallenge(codeChallengeId)
    }

    /**
     * Fetch info for the given challenge
     */
    fun fetchCodeChallenge(codeChallengeId: String) {
        mChallengeService.fetchCodeChallenge(codeChallengeId)
    }
}