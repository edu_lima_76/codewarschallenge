package com.eduardolima.codewarschallenge.activities.userinfo

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import com.eduardolima.codewarschallenge.R
import com.eduardolima.codewarschallenge.activities.BaseActivity
import com.eduardolima.codewarschallenge.fragments.authoredchallenges.AuthoredChallengesFragment
import com.eduardolima.codewarschallenge.fragments.completedchallenges.CompletedChallengesFragment


class UserInfoActivity : BaseActivity() {

    private lateinit var toolbar: ActionBar
    private lateinit var mUserId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_info)

        checkExtras()

        toolbar = supportActionBar!!
        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNavigation.selectedItemId = R.id.navigationCompletedChallenges
    }

    private fun checkExtras() {
        mUserId = intent!!.extras!!.getString(INTENT_USER_ID)!!
    }

    /**
     * Bottom bar navigation item selected listener
     */
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigationCompletedChallenges -> {
                toolbar.title = getString(R.string.menu_completed_challenges)

                val completedChallengesFragment = CompletedChallengesFragment.newInstance()
                completedChallengesFragment.setUserId(mUserId)

                openFragment(completedChallengesFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigationAuthoredChallenges -> {
                toolbar.title = getString(R.string.menu_authored_challenges)

                val authoredChallengesFragment = AuthoredChallengesFragment.newInstance()
                authoredChallengesFragment.setUserId(mUserId)

                openFragment(authoredChallengesFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}