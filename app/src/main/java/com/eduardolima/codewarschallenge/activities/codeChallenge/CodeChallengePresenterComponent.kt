package com.eduardolima.codewarschallenge.activities.codeChallenge

import com.eduardolima.codewarschallenge.shared.services.challenge.ChallengeServiceModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(ChallengeServiceModule::class))
interface CodeChallengePresenterComponent {
    fun inject(presenter: CodeChallengePresenter)
}