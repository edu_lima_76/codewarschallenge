package com.eduardolima.codewarschallenge.activities.codeChallenge

import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(CodeChallengePresenterModule::class))
interface CodeChallengeActivityComponent {
    fun inject(activity: CodeChallengeActivity)
}