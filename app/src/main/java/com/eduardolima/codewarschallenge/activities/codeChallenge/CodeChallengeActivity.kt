package com.eduardolima.codewarschallenge.activities.codeChallenge

import android.arch.lifecycle.Observer
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.ActionBar
import com.eduardolima.codewarschallenge.R
import com.eduardolima.codewarschallenge.activities.BaseActivity
import com.eduardolima.codewarschallenge.shared.database.pojo.codechallenge.CodeChallenge
import com.eduardolima.codewarschallenge.shared.utils.ErrorMap
import com.eduardolima.codewarschallenge.shared.views.View
import kotlinx.android.synthetic.main.activity_code_challenge.*
import kotlinx.android.synthetic.main.error_layout.*
import javax.inject.Inject

class CodeChallengeActivity : BaseActivity(), View {

    @Inject
    lateinit var mCodeChallengePresenter: CodeChallengePresenter

    private lateinit var toolbar: ActionBar
    private lateinit var mChallengeId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerCodeChallengeActivityComponent.builder()
                .codeChallengePresenterModule(CodeChallengePresenterModule(application))
                .build()
                .inject(this)

        setContentView(R.layout.activity_code_challenge)
        checkExtras()
        toolbar = supportActionBar!!
    }

    private fun checkExtras() {
        mChallengeId = intent!!.extras!!.getString(INTENT_CHALLENGE_ID)!!
    }

    override fun onResume() {
        super.onResume()
        mCodeChallengePresenter.subscribe(this)
        fetchCodeChallengeInfo()
        prepareView()
    }

    override fun onPause() {
        super.onPause()
        mCodeChallengePresenter.unsubscribe()
    }

    /**
     * Fetch the info corresponding to the previous selected code challenge
     */
    private fun fetchCodeChallengeInfo() {
        hideErrorLayout()
        mCodeChallengePresenter.fetchCodeChallenge(mChallengeId)
    }

    /**
     * Prepare view elements
     */
    private fun prepareView() {
        errorBtnRetry.setOnClickListener { fetchCodeChallengeInfo() }
    }

    /**
     * Updating code challenge info every time LiveData is received
     */
    private fun updateCodeChallenge() {
        mCodeChallengePresenter.getCodeChallenge(mChallengeId).observe(this, Observer { updateView(it!!) })
    }

    /**
     * Update the view according to the received info
     */
    private fun updateView(codeChallenge: CodeChallenge) {
        toolbar.title = codeChallenge.name

        publishAtTextView.text = codeChallenge.publishedAt.split("T")[0]
        createdAtTextView.text = codeChallenge.createdAt.split("T")[0]
        createdByTextView.text = codeChallenge.createdBy.username
        descriptionTextView.text = codeChallenge.description
        totalAttemptsTextView.text = codeChallenge.totalAttempts.toString()
        totalCompletedTextView.text = codeChallenge.totalCompleted.toString()
        totalStarsTextView.text = codeChallenge.totalStars.toString()

        challengeUrl.setOnClickListener {
            val target = Intent(Intent.ACTION_VIEW)
            target.data = Uri.parse(codeChallenge.url)
            target.flags = Intent.FLAG_ACTIVITY_NO_HISTORY

            val intent = Intent.createChooser(target, getString(R.string.challenge_web_page))
            try {
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }
        }

        codeChallengeContainer.visibility = android.view.View.VISIBLE
    }

    override fun onActionFail(action: Int, errorCode: String?) {
        showErrorLayout(getString(ErrorMap.getErrorMessage(errorCode!!)))
    }

    override fun onActionSuccess(action: Int) {
        mainProgress.visibility = android.view.View.GONE
        updateCodeChallenge()
    }

    private fun showErrorLayout(errorMessage: String?) {
        if (errorLayout.visibility == android.view.View.GONE) {
            errorLayout.visibility = android.view.View.VISIBLE
            mainProgress.visibility = android.view.View.GONE

            if (errorMessage != null)
                errorCauseTextView.text = errorMessage
        }
    }

    private fun hideErrorLayout() {
        if (errorLayout.visibility == android.view.View.VISIBLE) {
            errorLayout.visibility = android.view.View.GONE
            mainProgress.visibility = android.view.View.VISIBLE
        }
    }
}
