package com.eduardolima.codewarschallenge.activities.main

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.eduardolima.codewarschallenge.R
import com.eduardolima.codewarschallenge.activities.BaseActivity
import com.eduardolima.codewarschallenge.adapters.UserItemAdapter
import com.eduardolima.codewarschallenge.shared.components.callback.ErrorDialogCallback
import com.eduardolima.codewarschallenge.shared.database.pojo.User
import com.eduardolima.codewarschallenge.shared.services.Error
import com.eduardolima.codewarschallenge.shared.utils.ErrorMap
import com.eduardolima.codewarschallenge.shared.views.View
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), View, ErrorDialogCallback {

    @Inject
    lateinit var mMainPresenter: MainPresenter

    var mUsersList: MutableList<User> = ArrayList()

    lateinit var mUserAdapter: UserItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerMainActivityComponent.builder()
                .mainPresenterModule(MainPresenterModule(application))
                .build()
                .inject(this)

        setContentView(R.layout.activity_main)

    }

    override fun onResume() {
        super.onResume()
        mMainPresenter.subscribe(this)

        prepareView()
        updateUserList()
    }

    override fun onPause() {
        super.onPause()
        mMainPresenter.unsubscribe()
    }

    /**
     * Prepare view elements
     */
    private fun prepareView() {
        searchUser.setOnClickListener({
            mMainPresenter.fetchUserInfo(userEditText.text.toString())
            listOfUsersRecyclerView.visibility = android.view.View.GONE
            mainProgress.visibility = android.view.View.VISIBLE

            hideKeyboard(userEditText)
        })

        reorderByRankBtn.setOnClickListener {
            reorderUsersByRank()
        }

        mUserAdapter = UserItemAdapter(mListOfUsers = mUsersList,
                clickListener = { selectedUser: User -> userItemClick(selectedUser) })
        val layoutManager = LinearLayoutManager(this)
        listOfUsersRecyclerView.layoutManager = layoutManager
        listOfUsersRecyclerView.adapter = mUserAdapter
    }

    /**
     * Updating user list every time LiveData is received
     */
    private fun updateUserList() {
        mMainPresenter.getListOfUsers().observe(this, Observer { updateView(it!!) })
    }

    /**
     * Update the view according to the received info
     */
    private fun updateView(newUsersList: List<User>) {
        listOfUsersRecyclerView.visibility = android.view.View.VISIBLE
        mainProgress.visibility = android.view.View.GONE

        userEditText.setText("")
        userEditText.text.clear()
        mUsersList = newUsersList.toMutableList()
        mUserAdapter.setUsers(mUsersList)
    }

    override fun onActionFail(action: Int, @Error.Code errorCode: String?) {

        listOfUsersRecyclerView.visibility = android.view.View.VISIBLE
        mainProgress.visibility = android.view.View.GONE

        showErrorDialog(getString(R.string.error_dialog_title),
                getString(ErrorMap.getErrorMessage(errorCode!!)),
                this)
    }

    override fun onActionSuccess(action: Int) {}

    /**
     * Navigate to user info after click on it
     */
    private fun userItemClick(selectedUser: User) {
        navigateToUserInfo(selectedUser)
    }

    /**
     * Retry fetch user after previous request failed
     */
    override fun onRetryClick() {
        mMainPresenter.fetchUserInfo(userEditText.text.toString())
    }

    /**
     * Order the user list by rank
     */
    private fun reorderUsersByRank() {
        val sortedList = mUsersList.sortedWith(compareBy({ it.rank }))
        mUserAdapter.setUsers(sortedList)
    }
}
