package com.eduardolima.codewarschallenge.activities.main

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class MainPresenterModule(var mApp: Application) {
    @Provides
    @Singleton
    fun provideMainActivityPresenter(): MainPresenter {
        return MainPresenter(mApp)
    }
}