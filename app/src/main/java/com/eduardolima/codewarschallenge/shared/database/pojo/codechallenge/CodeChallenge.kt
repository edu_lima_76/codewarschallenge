package com.eduardolima.codewarschallenge.shared.database.pojo.codechallenge

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
data class CodeChallenge(
        @PrimaryKey
        @JsonProperty("id")
        val id: String,
        @JsonProperty("name")
        val name: String,
        @JsonProperty("slug")
        val slug: String,
        @JsonProperty("category")
        val category: String,
        @JsonProperty("publishedAt")
        val publishedAt: String,
        @JsonProperty("approvedAt")
        val approvedAt: String?,
        @JsonProperty("languages")
        val languages: List<String>,
        @JsonProperty("url")
        val url: String,
        @JsonProperty("createdAt")
        val createdAt: String,
        @JsonProperty("createdBy")
        @Embedded
        val createdBy: CodeChallengeUser,
        @JsonProperty("description")
        val description: String,
        @JsonProperty("totalAttempts")
        val totalAttempts: Int,
        @JsonProperty("totalCompleted")
        val totalCompleted: Int,
        @JsonProperty("totalStars")
        val totalStars: Int,
        @JsonProperty("voteScore")
        val voteScore: Int
)