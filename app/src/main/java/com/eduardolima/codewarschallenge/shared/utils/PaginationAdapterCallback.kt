package com.eduardolima.codewarschallenge.shared.utils


interface PaginationAdapterCallback {
    fun retryPageLoad()
}