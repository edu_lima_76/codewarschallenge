package com.eduardolima.codewarschallenge.shared.services


class Event(var type: Int, var action: Int, var error: String?) {

    companion object {
        internal const val TYPE_ERROR = 1000
        internal const val TYPE_SUCCESS = 1001
    }

    fun isError(): Boolean {
        return type == TYPE_ERROR
    }
}