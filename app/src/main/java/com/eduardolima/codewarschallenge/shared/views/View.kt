package com.eduardolima.codewarschallenge.shared.views

import com.eduardolima.codewarschallenge.shared.services.Error


interface View {
    fun onActionFail(action: Int, @Error.Code errorCode: String?)
    fun onActionSuccess(action: Int)
}
