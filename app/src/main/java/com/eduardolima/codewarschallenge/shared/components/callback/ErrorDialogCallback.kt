package com.eduardolima.codewarschallenge.shared.components.callback


interface ErrorDialogCallback {
    fun onRetryClick()
}