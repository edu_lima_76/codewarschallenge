package com.eduardolima.codewarschallenge.shared.client

import com.eduardolima.codewarschallenge.shared.database.pojo.User
import com.eduardolima.codewarschallenge.shared.database.pojo.authoredchallenges.UserAuthoredChallenges
import com.eduardolima.codewarschallenge.shared.database.pojo.codechallenge.CodeChallenge
import com.eduardolima.codewarschallenge.shared.database.pojo.completedchallenges.UserCompletedChallenges
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ICodewarsApi {

    @GET("users/{userId}")
    fun fetchUserInfo(@Path("userId") userId: String): Observable<Response<User>>

    @GET("users/{userId}/code-challenges/completed")
    fun fetchUserCompletedChallenges(@Path("userId") userId: String,
                                     @Query("page") page: Int): Observable<Response<UserCompletedChallenges>>

    @GET("users/{userId}/code-challenges/authored")
    fun fetchUserAuthoredChallenges(@Path("userId") userId: String,
                                    @Query("page") page: Int): Observable<Response<UserAuthoredChallenges>>

    @GET("code-challenges/{challengeId}")
    fun fetchCodeChallenge(@Path("challengeId") challengeId: String): Observable<Response<CodeChallenge>>
}