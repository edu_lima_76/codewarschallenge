package com.eduardolima.codewarschallenge.shared.presenter

import android.util.Log
import com.eduardolima.codewarschallenge.shared.services.Error
import com.eduardolima.codewarschallenge.shared.services.Event
import com.eduardolima.codewarschallenge.shared.views.View
import io.reactivex.annotations.NonNull


abstract class BasePresenter {

    val TAG = BasePresenter::class.java.simpleName

    var mView: View? = null

    abstract fun processSuccess(action: Int)

    protected open fun subscribe(@NonNull view: View) {
        mView = view
    }

    protected open fun unsubscribe() {
        mView = null
    }

    protected fun processEvent(evt: Event) {
        if (evt.isError()) {
            processError(evt.action, evt.error)
        } else {
            processSuccess(evt.action)
        }
    }

    open fun processError(action: Int, @Error.Code errorCode: String?) {
        Log.d(TAG, "Action failed. handle error : $errorCode");
    }
}