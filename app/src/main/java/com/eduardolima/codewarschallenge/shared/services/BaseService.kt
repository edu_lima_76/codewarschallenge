package com.eduardolima.codewarschallenge.shared.services

import android.support.annotation.IntDef
import com.eduardolima.codewarschallenge.shared.services.Error.GENERIC_ERROR_CODE
import com.eduardolima.codewarschallenge.shared.services.Error.NO_NETWORK_CONNECTION
import com.eduardolima.codewarschallenge.shared.services.Error.USER_NOT_FOUND
import com.eduardolima.codewarschallenge.shared.services.Event.Companion.TYPE_ERROR
import com.eduardolima.codewarschallenge.shared.services.Event.Companion.TYPE_SUCCESS
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import javax.inject.Inject


open class BaseService {

    companion object {
        const val HTTP_NOT_FOUND = 404

        const val ACTION_FETCH_USER: Int = 1000
        const val ACTION_FETCH_USER_COMPLETED_CHALLENGES: Int = 1001
        const val ACTION_FETCH_USER_AUTHORED_CHALLENGES: Int = 1002
        const val ACTION_FETCH_CODE_CHALLENGE: Int = 1003

        @IntDef(ACTION_FETCH_USER,
                ACTION_FETCH_USER_COMPLETED_CHALLENGES,
                ACTION_FETCH_USER_AUTHORED_CHALLENGES)
        internal annotation class ServiceActions
    }

    @Inject
    lateinit var mStream: PublishSubject<Event>

    fun getStream(): Observable<Event> {
        return mStream
    }

    fun onSuccess(@ServiceActions action: Int) {
        if (mStream.hasObservers()) {
            mStream.onNext(Event(TYPE_SUCCESS, action, null))
        }
    }

    fun onError(@ServiceActions action: Int, @Error.Code errorCode: String) {
        if (mStream.hasObservers()) {
            mStream.onNext(Event(TYPE_ERROR, action, errorCode))
        }
    }

    fun handleUnsuccessfulResponse(@ServiceActions action: Int, responseCode: Int) {
        when (responseCode) {
            HTTP_NOT_FOUND -> {
                if (action == ACTION_FETCH_USER) {
                    onError(action, USER_NOT_FOUND)
                } else {
                    onError(action, GENERIC_ERROR_CODE)
                }
            }
            else -> onError(action, GENERIC_ERROR_CODE)
        }
    }

    protected fun handleOnError(@ServiceActions action: Int, throwable: Throwable?) {
        if (throwable == null || throwable is HttpException) {
            throwable!!.printStackTrace()
            onError(action, GENERIC_ERROR_CODE)
        } else {
            onError(action, NO_NETWORK_CONNECTION)
        }
    }
}