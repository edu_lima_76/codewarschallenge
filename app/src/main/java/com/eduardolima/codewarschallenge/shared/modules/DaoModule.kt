package com.eduardolima.codewarschallenge.shared.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.eduardolima.codewarschallenge.shared.database.CodewarsDatabase
import com.eduardolima.codewarschallenge.shared.database.dao.CodeChallengeDao
import com.eduardolima.codewarschallenge.shared.database.dao.UserDao
import dagger.Module
import dagger.Provides


@Module(includes = arrayOf(ContextModule::class))
object DaoModule {

    private const val DATABASE_NAME = "CodewarsChallenge"

    @Provides
    fun provideCodewarsDatabase(context: Context): CodewarsDatabase {
        return Room.databaseBuilder(context.applicationContext,
                CodewarsDatabase::class.java,
                DATABASE_NAME).build()
    }

    @Provides
    @JvmStatic
    fun provideUserDao(db: CodewarsDatabase): UserDao {
        return db.userDao()
    }

    @Provides
    @JvmStatic
    fun provideCodeChallengeDao(db: CodewarsDatabase): CodeChallengeDao {
        return db.codeChallengeDao()
    }
}