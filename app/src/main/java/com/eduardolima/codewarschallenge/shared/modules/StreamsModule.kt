package com.eduardolima.codewarschallenge.shared.modules

import com.eduardolima.codewarschallenge.shared.services.Event
import dagger.Module
import dagger.Provides
import io.reactivex.subjects.PublishSubject


@Module
object StreamsModule {

    @Provides
    fun provideStream() : PublishSubject<Event> {
        return PublishSubject.create()
    }
}