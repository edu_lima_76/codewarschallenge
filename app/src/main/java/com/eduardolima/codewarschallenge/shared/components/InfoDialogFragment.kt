package com.eduardolima.codewarschallenge.shared.components

import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eduardolima.codewarschallenge.R
import kotlinx.android.synthetic.main.info_dialog.*


class InfoDialogFragment : DialogFragment() {

    companion object {
        const val KEY_DIALOG_TITLE = "dialog_title"
        const val KEY_DIALOG_MESSAGE = "dialog_message"

        fun newInstance(dialogTitle: String,
                        dialogMessage: String): InfoDialogFragment {
            val fragment = InfoDialogFragment()

            val args = Bundle()
            args.putString(KEY_DIALOG_TITLE, dialogTitle)
            args.putString(KEY_DIALOG_MESSAGE, dialogMessage)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(R.layout.info_dialog, container)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dialogTitle = arguments.getString(KEY_DIALOG_TITLE)
        val dialogMessage = arguments.getString(KEY_DIALOG_MESSAGE)

        dialogTitleTextView.text = dialogTitle
        dialogMessageTextView.text = dialogMessage

        okAction.setOnClickListener { dismiss() }
    }
}