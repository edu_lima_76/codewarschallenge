package com.eduardolima.codewarschallenge.shared.services.challenge

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ChallengeServiceModule(var mApp: Application) {
    @Provides
    @Singleton
    fun provideChallengeService(): ChallengeService {
        return ChallengeService(mApp)
    }
}