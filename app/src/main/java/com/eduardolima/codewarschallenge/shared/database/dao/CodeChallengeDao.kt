package com.eduardolima.codewarschallenge.shared.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.eduardolima.codewarschallenge.shared.database.pojo.codechallenge.CodeChallenge


/**
 * Dao for the code challenges info
 */
@Dao
interface CodeChallengeDao {
    @Query("SELECT * FROM CodeChallenge WHERE id = :codeChallengeId")
    fun getCodeChallengeInfo(codeChallengeId: String): LiveData<CodeChallenge>

    @Insert(onConflict = REPLACE)
    fun insertCodeChallenge(codeChallenge: CodeChallenge)
}