package com.eduardolima.codewarschallenge.shared.database.pojo.completedchallenges

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class UserCompletedChallenges(
        @JsonProperty("totalPages")
        val totalPages: Int,
        @JsonProperty("totalItems")
        val totalItems: Int,
        @JsonProperty("data")
        val completedChallenges: List<CompletedChallenge>?
)