package com.eduardolima.codewarschallenge.shared.services.user

import com.eduardolima.codewarschallenge.shared.modules.ApiModule
import com.eduardolima.codewarschallenge.shared.modules.DaoModule
import com.eduardolima.codewarschallenge.shared.modules.StreamsModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(
        ApiModule::class,
        StreamsModule::class,
        DaoModule::class))
interface UserServiceComponent {
    fun inject(service: UserService)
}