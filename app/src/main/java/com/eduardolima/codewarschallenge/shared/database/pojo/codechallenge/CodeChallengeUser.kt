package com.eduardolima.codewarschallenge.shared.database.pojo.codechallenge

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
class CodeChallengeUser(
        @JsonProperty("username")
        val username: String,
        @JsonProperty("url")
        val urlUser: String
)