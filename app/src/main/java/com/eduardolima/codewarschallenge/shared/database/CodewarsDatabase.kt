package com.eduardolima.codewarschallenge.shared.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.eduardolima.codewarschallenge.shared.database.converter.DatabaseConverters
import com.eduardolima.codewarschallenge.shared.database.dao.CodeChallengeDao
import com.eduardolima.codewarschallenge.shared.database.dao.UserDao
import com.eduardolima.codewarschallenge.shared.database.pojo.User
import com.eduardolima.codewarschallenge.shared.database.pojo.codechallenge.CodeChallenge


/**
 * Codewars database
 */
@Database(entities = arrayOf(User::class, CodeChallenge::class), version = 1)
@TypeConverters(DatabaseConverters::class)
abstract class CodewarsDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun codeChallengeDao(): CodeChallengeDao
}