package com.eduardolima.codewarschallenge.shared.modules

import android.util.Log
import com.eduardolima.codewarschallenge.BuildConfig
import com.eduardolima.codewarschallenge.shared.client.ICodewarsApi
import com.eduardolima.codewarschallenge.shared.client.RestClientFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
object ApiModule {

    private const val URL_CODEWARS_API = "https://www.codewars.com/api/v1/"
    private const val READ_TIMEPOUT: Long = 60
    private const val CONNECT_TIMEOUT: Long = 60

    lateinit var mOkHttpClient: OkHttpClient

    @Provides
    @Singleton
    fun provideCodewarsChallengeAPI(client: OkHttpClient): ICodewarsApi {
        return RestClientFactory.createRetrofitRestClient(URL_CODEWARS_API, client)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor(
                HttpLoggingInterceptor.Logger { message ->
                    Log.i("HttpLoggingInterceptor:", message)
                })
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val clientBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            clientBuilder.addInterceptor(loggingInterceptor)
        }

        mOkHttpClient = clientBuilder
                .readTimeout(READ_TIMEPOUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .build()

        return mOkHttpClient
    }
}
