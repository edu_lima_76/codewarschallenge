package com.eduardolima.codewarschallenge.shared.database.converter

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*


/**
 * Database converters
 */
open class DatabaseConverters {
    private var gson = Gson()

    /**
     * Convert from String to List<String>
     */
    @TypeConverter
    fun stringToStringList(data: String?): List<String> {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(data, listType)
    }

    /**
     * Convert from List<String> to String
     */
    @TypeConverter
    fun stringListToString(someObjects: List<String>): String {
        return gson.toJson(someObjects)
    }
}