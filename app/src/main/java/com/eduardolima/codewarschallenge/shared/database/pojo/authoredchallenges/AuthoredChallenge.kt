package com.eduardolima.codewarschallenge.shared.database.pojo.authoredchallenges

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
class AuthoredChallenge(
        @JsonProperty("id")
        val id: String,
        @JsonProperty("name")
        val name: String
)