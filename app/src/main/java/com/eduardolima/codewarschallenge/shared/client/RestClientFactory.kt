package com.eduardolima.codewarschallenge.shared.client

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory


object RestClientFactory {

    /**
     * Creates a retrofit service from an arbitrary class
     * @param endPoint REST endpoint url
     * @return retrofit service with defined endpoint
     */
    fun createRetrofitRestClient(endPoint : String, okHttpClient: OkHttpClient) : ICodewarsApi {
        val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(endPoint)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build()

        return retrofit.create(ICodewarsApi::class.java)
    }
}