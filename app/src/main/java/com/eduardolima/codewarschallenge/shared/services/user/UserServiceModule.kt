package com.eduardolima.codewarschallenge.shared.services.user

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class UserServiceModule(var mApp: Application) {
    @Provides
    @Singleton
    fun provideUserService(): UserService {
        return UserService(mApp)
    }
}