package com.eduardolima.codewarschallenge.shared.components

import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eduardolima.codewarschallenge.R
import com.eduardolima.codewarschallenge.shared.components.callback.ErrorDialogCallback
import kotlinx.android.synthetic.main.error_dialog.*


class ErrorDialogFragment : DialogFragment() {

    private lateinit var mCallback: ErrorDialogCallback

    companion object {
        const val KEY_DIALOG_TITLE = "dialog_title"
        const val KEY_DIALOG_MESSAGE = "dialog_message"

        fun newInstance(dialogTitle: String,
                        dialogMessage: String,
                        callback: ErrorDialogCallback): ErrorDialogFragment {
            val fragment = ErrorDialogFragment()

            fragment.mCallback = callback

            val args = Bundle()
            args.putString(KEY_DIALOG_TITLE, dialogTitle)
            args.putString(KEY_DIALOG_MESSAGE, dialogMessage)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(R.layout.error_dialog, container)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dialogTitle = arguments.getString(KEY_DIALOG_TITLE)
        val dialogMessage = arguments.getString(KEY_DIALOG_MESSAGE)

        dialogTitleTextView.text = dialogTitle
        dialogMessageTextView.text = dialogMessage

        cancelAction.setOnClickListener { dismiss() }
        retryAction.setOnClickListener {
            dismiss()
            mCallback.onRetryClick()
        }
    }
}