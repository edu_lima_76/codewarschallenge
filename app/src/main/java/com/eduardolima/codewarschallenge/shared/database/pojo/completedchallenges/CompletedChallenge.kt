package com.eduardolima.codewarschallenge.shared.database.pojo.completedchallenges

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
class CompletedChallenge(
        @JsonProperty("id")
        val id: String?,
        @JsonProperty("name")
        val name: String?
)