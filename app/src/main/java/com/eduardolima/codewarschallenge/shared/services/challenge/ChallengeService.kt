package com.eduardolima.codewarschallenge.shared.services.challenge

import android.app.Application
import android.arch.lifecycle.LiveData
import com.eduardolima.codewarschallenge.shared.client.ICodewarsApi
import com.eduardolima.codewarschallenge.shared.database.DbWorkerThread
import com.eduardolima.codewarschallenge.shared.database.dao.CodeChallengeDao
import com.eduardolima.codewarschallenge.shared.database.pojo.codechallenge.CodeChallenge
import com.eduardolima.codewarschallenge.shared.modules.ApiModule
import com.eduardolima.codewarschallenge.shared.modules.ContextModule
import com.eduardolima.codewarschallenge.shared.modules.DaoModule
import com.eduardolima.codewarschallenge.shared.modules.StreamsModule
import com.eduardolima.codewarschallenge.shared.services.BaseService
import com.eduardolima.codewarschallenge.shared.services.Error
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject


class ChallengeService(var mApp: Application) : BaseService() {

    @Inject
    lateinit var mICodewarsApi: ICodewarsApi

    @Inject
    lateinit var mCodeChallengeDao: CodeChallengeDao

    private var mDbWorkerThread: DbWorkerThread

    init {
        this(DaggerChallengeServiceComponent.builder()
                .apiModule(ApiModule)
                .streamsModule(StreamsModule)
                .contextModule(ContextModule(mApp.applicationContext))
                .daoModule(DaoModule)
                .build()
                .inject(this))

        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()
    }

    private operator fun invoke(inject: Unit) {}

    fun getCodeChallenge(codeChallengeId: String): LiveData<CodeChallenge> {
        return mCodeChallengeDao.getCodeChallengeInfo(codeChallengeId)
    }

    /**
     * Fetch code challenge info from selected challenge
     */
    fun fetchCodeChallenge(codeChallengeId: String) {
        mICodewarsApi.fetchCodeChallenge(codeChallengeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe({ codeChallengeResponse: Response<CodeChallenge>? -> handleCodeChallengeResponse(codeChallengeResponse) },
                        { throwable: Throwable? -> handleOnError(ACTION_FETCH_CODE_CHALLENGE, throwable) })

    }

    /**
     * Handle code challenges reponse
     */
    private fun handleCodeChallengeResponse(response: Response<CodeChallenge>?) {
        when {
            response == null -> onError(ACTION_FETCH_CODE_CHALLENGE, Error.GENERIC_ERROR_CODE)
            !response.isSuccessful -> handleUnsuccessfulResponse(ACTION_FETCH_CODE_CHALLENGE, response.code())
            else -> {
                val codeChallenge = response.body()!!

                val task = Runnable { mCodeChallengeDao.insertCodeChallenge(codeChallenge) }
                mDbWorkerThread.postTask(task)
                onSuccess(ACTION_FETCH_CODE_CHALLENGE)
            }
        }
    }
}