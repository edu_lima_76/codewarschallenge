package com.eduardolima.codewarschallenge.shared.database.pojo

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
data class User(
        @PrimaryKey
        val username: String,
        val name: String?,
        val honor: Int?,
        val rank: Int?,
        var createdTimestamp: Long?
) {
    companion object {
        @JsonCreator
        @JvmStatic
        fun create(@JsonProperty("username")
                   username: String,
                   @JsonProperty("name")
                   name: String?,
                   @JsonProperty("honor")
                   honor: Int?,
                   @JsonProperty("leaderboardPosition")
                   rank: Int?): User {
            return User(username, name, honor, rank, System.currentTimeMillis() / 1000)
        }
    }
}