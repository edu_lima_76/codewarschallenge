package com.eduardolima.codewarschallenge.shared.services.user

import android.app.Application
import android.arch.lifecycle.LiveData
import com.eduardolima.codewarschallenge.shared.client.ICodewarsApi
import com.eduardolima.codewarschallenge.shared.database.DbWorkerThread
import com.eduardolima.codewarschallenge.shared.database.dao.UserDao
import com.eduardolima.codewarschallenge.shared.database.pojo.User
import com.eduardolima.codewarschallenge.shared.database.pojo.authoredchallenges.UserAuthoredChallenges
import com.eduardolima.codewarschallenge.shared.database.pojo.completedchallenges.UserCompletedChallenges
import com.eduardolima.codewarschallenge.shared.modules.ApiModule
import com.eduardolima.codewarschallenge.shared.modules.ContextModule
import com.eduardolima.codewarschallenge.shared.modules.DaoModule
import com.eduardolima.codewarschallenge.shared.modules.StreamsModule
import com.eduardolima.codewarschallenge.shared.services.BaseService
import com.eduardolima.codewarschallenge.shared.services.Error.GENERIC_ERROR_CODE
import com.eduardolima.codewarschallenge.shared.services.Error.USER_NOT_FOUND
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject


class UserService(var mApp: Application) : BaseService() {

    @Inject
    lateinit var mICodewarsApi: ICodewarsApi

    @Inject
    lateinit var mUserDao: UserDao

    private var mDbWorkerThread: DbWorkerThread

    private lateinit var mUserId: String

    var mUserCompletedChallenges: UserCompletedChallenges? = null
    var mUserAuthoredChallenges: UserAuthoredChallenges? = null

    init {
        this(DaggerUserServiceComponent.builder()
                .apiModule(ApiModule)
                .streamsModule(StreamsModule)
                .contextModule(ContextModule(mApp.applicationContext))
                .daoModule(DaoModule)
                .build()
                .inject(this))

        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()
    }

    private operator fun invoke(inject: Unit) {}

    fun getListOfUsers(): LiveData<List<User>> {
        return mUserDao.getLastSearchUsers
    }

    /**
     * Fetch user info
     */
    fun fetchUserInfo(userId: String) {
        mICodewarsApi.fetchUserInfo(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe({ userResponse: Response<User>? -> handleUserInfoResponse(userResponse) },
                        { throwable: Throwable? -> handleOnError(ACTION_FETCH_USER, throwable) })
    }

    /**
     * Handle user info response
     */
    private fun handleUserInfoResponse(response: Response<User>?) {
        when {
            response == null -> onError(ACTION_FETCH_USER, GENERIC_ERROR_CODE)
            !response.isSuccessful -> handleUnsuccessfulResponse(ACTION_FETCH_USER, response.code())
            else -> {
                val user: User = response.body()!!
                if (user.name == null) {
                    onError(ACTION_FETCH_USER, USER_NOT_FOUND)
                }

                val task = Runnable { mUserDao.insertUser(user) }
                mDbWorkerThread.postTask(task)
                onSuccess(ACTION_FETCH_USER)
            }
        }
    }

    /**
     * Fetch completed challenges from selected info
     */
    fun fetchUserCompletedChallenges(userId: String, page: Int) {
        mUserId = userId
        mICodewarsApi.fetchUserCompletedChallenges(userId, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe({ userCompletedChallenges: Response<UserCompletedChallenges>?
                    ->
                    handleUserCompletedChallengesResponse(userCompletedChallenges)
                },
                        { throwable: Throwable? -> handleOnError(ACTION_FETCH_USER_COMPLETED_CHALLENGES, throwable) })
    }

    fun getUserCompletedChallenges(): UserCompletedChallenges? {
        return mUserCompletedChallenges
    }

    /**
     * Handle completed challenges response
     */
    private fun handleUserCompletedChallengesResponse(response: Response<UserCompletedChallenges>?) {
        when {
            response == null -> onError(ACTION_FETCH_USER_COMPLETED_CHALLENGES, GENERIC_ERROR_CODE)
            !response.isSuccessful -> handleUnsuccessfulResponse(ACTION_FETCH_USER_COMPLETED_CHALLENGES, response.code())
            else -> {
                val userCompletedChallenges = response.body()!!
                mUserCompletedChallenges = userCompletedChallenges
                onSuccess(ACTION_FETCH_USER_COMPLETED_CHALLENGES)
            }
        }
    }

    /**
     * Fetch authored challenges from selected info
     */
    fun fetchUserAuthoredChallenges(userId: String, page: Int) {
        mUserId = userId
        mICodewarsApi.fetchUserAuthoredChallenges(userId, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe({ userAuthoredChallengesResponse: Response<UserAuthoredChallenges>?
                    ->
                    handleUserAuthoredChallengesResponse(userAuthoredChallengesResponse)
                },
                        { throwable: Throwable? -> handleOnError(ACTION_FETCH_USER_AUTHORED_CHALLENGES, throwable) })
    }

    fun getUserAuthoredChallenges(): UserAuthoredChallenges? {
        return mUserAuthoredChallenges
    }

    /**
     * Handle completed challenges response
     */
    private fun handleUserAuthoredChallengesResponse(response: Response<UserAuthoredChallenges>?) {
        when {
            response == null -> onError(ACTION_FETCH_USER_AUTHORED_CHALLENGES, GENERIC_ERROR_CODE)
            !response.isSuccessful -> handleUnsuccessfulResponse(ACTION_FETCH_USER_AUTHORED_CHALLENGES, response.code())
            else -> {
                val userAuthoredChallenges = response.body()!!
                mUserAuthoredChallenges = userAuthoredChallenges
                onSuccess(ACTION_FETCH_USER_AUTHORED_CHALLENGES)
            }
        }
    }
}