package com.eduardolima.codewarschallenge.shared.services

import android.support.annotation.StringDef


object Error {
    const val GENERIC_ERROR_CODE = "generic_error_code"
    const val USER_NOT_FOUND = "user_not_found"
    const val NO_NETWORK_CONNECTION = "no_network_connection"

    @StringDef(USER_NOT_FOUND,
            NO_NETWORK_CONNECTION)
    annotation class Code
}