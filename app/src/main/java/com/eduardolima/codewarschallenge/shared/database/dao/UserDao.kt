package com.eduardolima.codewarschallenge.shared.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.eduardolima.codewarschallenge.shared.database.pojo.User
import com.eduardolima.codewarschallenge.shared.database.pojo.completedchallenges.UserCompletedChallenges


/**
 * Dao for the list of users
 */
@Dao
interface UserDao {
    @get:Query("SELECT * FROM User ORDER BY createdTimestamp DESC LIMIT 5")
    val getLastSearchUsers: LiveData<List<User>>

    @Insert(onConflict = REPLACE)
    fun insertUser(user: User)
}