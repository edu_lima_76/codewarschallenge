package com.eduardolima.codewarschallenge.shared.utils

import com.eduardolima.codewarschallenge.R
import com.eduardolima.codewarschallenge.shared.services.Error
import com.eduardolima.codewarschallenge.shared.services.Error.NO_NETWORK_CONNECTION
import com.eduardolima.codewarschallenge.shared.services.Error.USER_NOT_FOUND


object ErrorMap {

    fun getErrorMessage(@Error.Code errorCode: String): Int {
        return when (errorCode) {
            NO_NETWORK_CONNECTION -> R.string.error_no_network_connection
            USER_NOT_FOUND -> R.string.error_user_not_found
            else -> R.string.error_something_went_wrong
        }
    }

}