package com.eduardolima.codewarschallenge.fragments.completedchallenges

import com.eduardolima.codewarschallenge.shared.services.user.UserServiceModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(UserServiceModule::class))
interface CompletedChallengesPresenterComponent {
    fun inject(presenter: CompletedChallengesPresenter)
}