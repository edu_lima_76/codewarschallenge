package com.eduardolima.codewarschallenge.fragments.authoredchallenges

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AuthoredChallengesPresenterModule(var mApp: Application) {
    @Provides
    @Singleton
    fun provideAuthoredChallengesPresenter(): AuthoredChallengesPresenter {
        return AuthoredChallengesPresenter(mApp)
    }
}