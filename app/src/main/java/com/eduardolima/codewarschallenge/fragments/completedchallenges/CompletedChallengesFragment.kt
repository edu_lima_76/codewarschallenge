package com.eduardolima.codewarschallenge.fragments.completedchallenges

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eduardolima.codewarschallenge.R
import com.eduardolima.codewarschallenge.adapters.CompletedChallengesAdapter
import com.eduardolima.codewarschallenge.fragments.BaseFragment
import com.eduardolima.codewarschallenge.shared.database.pojo.completedchallenges.CompletedChallenge
import com.eduardolima.codewarschallenge.shared.database.pojo.completedchallenges.UserCompletedChallenges
import com.eduardolima.codewarschallenge.shared.services.Error
import com.eduardolima.codewarschallenge.shared.utils.ErrorMap
import com.eduardolima.codewarschallenge.shared.utils.PaginationAdapterCallback
import com.eduardolima.codewarschallenge.shared.utils.PaginationScrollListener
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.fragment_completed_challenges.*
import javax.inject.Inject


class CompletedChallengesFragment : BaseFragment(), PaginationAdapterCallback {

    private lateinit var mUserId: String
    private lateinit var mAdapter: CompletedChallengesAdapter

    private var mIsLoading = false
    private var mIsLastPage = false
    private var mLoadingFirstPageDate = false
    private var mCurrentPage = 0
    private var mTotalPages = 1

    @Inject
    lateinit var mCompletedChallengesPresenter: CompletedChallengesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        val app = activity!!.application
        DaggerCompletedChallengesFragmentComponent.builder()
                .completedChallengesPresenterModule(CompletedChallengesPresenterModule(app))
                .build()
                .inject(this)

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_completed_challenges, container, false)
    }

    companion object {
        fun newInstance(): CompletedChallengesFragment = CompletedChallengesFragment()
    }

    override fun onResume() {
        super.onResume()
        mCompletedChallengesPresenter.subscribe(this)
        fetchUserCompletedChallengesFirstPage()
        prepareView()
    }

    override fun onPause() {
        super.onPause()
        mCompletedChallengesPresenter.unsubscribe()
    }

    fun setUserId(userId: String) {
        this.mUserId = userId
    }

    /**
     * Fetch data for the first page
     */
    private fun fetchUserCompletedChallengesFirstPage() {
        mLoadingFirstPageDate = true
        mainProgress.visibility = View.VISIBLE
        mCompletedChallengesPresenter.fetchUserCompletedChallenges(mUserId, 0)
    }

    /**
     * Fetch data for next pages
     */
    private fun fetchUserCompletedChallengesNextPage() {
        mLoadingFirstPageDate = false
        mCompletedChallengesPresenter.fetchUserCompletedChallenges(mUserId, mCurrentPage)
    }

    /**
     * Prepare view elements
     */
    private fun prepareView() {
        errorBtnRetry.setOnClickListener({ retryFirstPageLoad() })

        mAdapter = CompletedChallengesAdapter(
                mItemClickListener = { selectedChallenge: CompletedChallenge -> itemClicked(selectedChallenge) },
                mContract = this)

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        completedChallengesRecycler.layoutManager = layoutManager
        completedChallengesRecycler.itemAnimator = DefaultItemAnimator()
        completedChallengesRecycler.adapter = mAdapter
        completedChallengesRecycler.addOnScrollListener(object : PaginationScrollListener(layoutManager) {
            override fun loadMoreItems() {
                mIsLoading = true
                mCurrentPage += 1
                fetchUserCompletedChallengesNextPage()
            }

            override fun getTotalPageCount(): Int {
                return mTotalPages
            }

            override fun isLastPage(): Boolean {
                return mIsLastPage
            }

            override fun isLoading(): Boolean {
                return mIsLoading
            }

        })

    }

    /**
     * Update the view with info from the first page fetch
     */
    private fun updateViewFirstPage(userCompletedChallenges: UserCompletedChallenges) {
        hideErrorLayout()

        mTotalPages = userCompletedChallenges.totalPages
        mainProgress.visibility = View.GONE
        mAdapter.addAllChallenges(userCompletedChallenges.completedChallenges!!)

        if (mCurrentPage <= mTotalPages) {
            mAdapter.addLoadingFooter()
        } else {
            mIsLastPage = true
            // no more pages available to be fetch
            showInfoDialog(getString(R.string.info_no_more_pages_title), getString(R.string.info_no_more_pages_description))
        }
    }

    /**
     * Update view by adding new data to the previous one, after reaching the bottom of the list
     */
    private fun updateViewOtherPages(userCompletedChallenges: UserCompletedChallenges) {
        mAdapter.removeLoadingFooter()
        mIsLoading = false

        mAdapter.addAllChallenges(userCompletedChallenges.completedChallenges!!)

        if (mCurrentPage != mTotalPages)
            mAdapter.addLoadingFooter()
        else {
            mIsLastPage = true
            // no more pages available to be fetch
            showInfoDialog(getString(R.string.info_no_more_pages_title), getString(R.string.info_no_more_pages_description))
        }
    }

    /**
     * Navigate to challenge info view after click on the item
     */
    private fun itemClicked(selectedChallenge: CompletedChallenge) {
        navigateToCodeChallenge(selectedChallenge.id!!)
    }

    /**
     * Retry fetch data after error occur on the first page collecting process
     */
    private fun retryFirstPageLoad() {
        hideErrorLayout()
        fetchUserCompletedChallengesFirstPage()
    }

    /**
     * Retry fecth data after error occur on the next pages collecting process
     */
    override fun retryPageLoad() {
        mAdapter.showRetry(false, null)
        fetchUserCompletedChallengesNextPage()
    }

    override fun onActionFail(action: Int, @Error.Code errorCode: String?) {
        if (mLoadingFirstPageDate)
            showErrorLayout(getString(ErrorMap.getErrorMessage(errorCode!!)))
        else
            mAdapter.showRetry(true, getString(ErrorMap.getErrorMessage(errorCode!!)))
    }

    override fun onActionSuccess(action: Int) {
        if (mLoadingFirstPageDate)
            updateViewFirstPage(mCompletedChallengesPresenter.getUserCompletedChallenges()!!)
        else
            updateViewOtherPages(mCompletedChallengesPresenter.getUserCompletedChallenges()!!)
    }

    private fun showErrorLayout(errorMessage: String?) {
        if (errorLayout.visibility == View.GONE) {
            errorLayout.visibility = View.VISIBLE
            mainProgress.visibility = View.GONE

            if (errorMessage != null)
                errorCauseTextView.text = errorMessage
        }
    }

    private fun hideErrorLayout() {
        if (errorLayout.visibility == View.VISIBLE) {
            errorLayout.visibility = View.GONE
            mainProgress.visibility = View.VISIBLE
        }
    }
}