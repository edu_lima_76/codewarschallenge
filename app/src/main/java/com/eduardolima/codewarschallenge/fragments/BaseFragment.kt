package com.eduardolima.codewarschallenge.fragments

import android.support.v4.app.Fragment
import com.eduardolima.codewarschallenge.activities.BaseActivity
import com.eduardolima.codewarschallenge.shared.views.View


open class BaseFragment : Fragment(), View {
    override fun onActionFail(action: Int, errorCode: String?) {
    }

    override fun onActionSuccess(action: Int) {
    }

    private fun getBaseActivity(): BaseActivity {
        return activity as BaseActivity
    }

    protected fun navigateToCodeChallenge(selectedChallengeId: String) {
        val baseActivity = getBaseActivity()
        baseActivity.navigateToCodeChallenge(selectedChallengeId)
    }

    fun showInfoDialog(dialogTitle: String,
                       dialogMessage: String) {
        val baseActivity = getBaseActivity()
        baseActivity.showInfoDialog(dialogTitle, dialogMessage)
    }
}