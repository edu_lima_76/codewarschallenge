package com.eduardolima.codewarschallenge.fragments.completedchallenges

import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(CompletedChallengesPresenterModule::class))
interface CompletedChallengesFragmentComponent {
    fun inject(fragment: CompletedChallengesFragment)
}