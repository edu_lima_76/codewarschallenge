package com.eduardolima.codewarschallenge.fragments.completedchallenges

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class CompletedChallengesPresenterModule(var mApp: Application) {
    @Provides
    @Singleton
    fun provideCompletedChallengesPresenter(): CompletedChallengesPresenter {
        return CompletedChallengesPresenter(mApp)
    }
}