package com.eduardolima.codewarschallenge.fragments.authoredchallenges

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eduardolima.codewarschallenge.R
import com.eduardolima.codewarschallenge.adapters.AuthoredChallengesAdapter
import com.eduardolima.codewarschallenge.fragments.BaseFragment
import com.eduardolima.codewarschallenge.shared.database.pojo.authoredchallenges.AuthoredChallenge
import com.eduardolima.codewarschallenge.shared.database.pojo.authoredchallenges.UserAuthoredChallenges
import com.eduardolima.codewarschallenge.shared.services.Error
import com.eduardolima.codewarschallenge.shared.utils.ErrorMap
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.fragment_authored_challenges.*
import javax.inject.Inject


class AuthoredChallengesFragment : BaseFragment() {

    @Inject
    lateinit var mAuthoredChallengesPresenter: AuthoredChallengesPresenter

    private lateinit var mAuthoredChallengeAdapter: AuthoredChallengesAdapter
    private lateinit var mUserId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        val app = activity!!.application
        DaggerAuthoredChallengesFragmentComponent.builder()
                .authoredChallengesPresenterModule(AuthoredChallengesPresenterModule(app))
                .build()
                .inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_authored_challenges, container, false)
    }

    companion object {
        fun newInstance(): AuthoredChallengesFragment = AuthoredChallengesFragment()
    }

    override fun onResume() {
        super.onResume()
        mAuthoredChallengesPresenter.subscribe(this)
        fetchUserAuthoredChallenges()
        prepareView()
    }

    override fun onPause() {
        super.onPause()
        mAuthoredChallengesPresenter.unsubscribe()
    }

    fun setUserId(userId: String) {
        this.mUserId = userId
    }

    private fun fetchUserAuthoredChallenges() {
        mAuthoredChallengesPresenter.fetchUserAuthoredChallenges(mUserId, 0)
    }

    /**
     * Prepare view elements
     */
    private fun prepareView() {
        mAuthoredChallengeAdapter = AuthoredChallengesAdapter { selectedChallenge: AuthoredChallenge
            ->
            itemClicked(selectedChallenge)
        }

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        authoredChallengesRecycler.layoutManager = layoutManager
        authoredChallengesRecycler.itemAnimator = DefaultItemAnimator()
        authoredChallengesRecycler.adapter = mAuthoredChallengeAdapter
    }

    /**
     * Navigate to challenge info view after click on the item
     */
    private fun itemClicked(selectedChallenge: AuthoredChallenge) {
        navigateToCodeChallenge(selectedChallenge.id)
    }

    override fun onActionFail(action: Int, @Error.Code errorCode: String?) {
        showErrorLayout(getString(ErrorMap.getErrorMessage(errorCode!!)))
    }

    override fun onActionSuccess(action: Int) {
        updateView(mAuthoredChallengesPresenter.getUserAuthoredChallenges()!!)
    }

    private fun updateView(userAuthoredChallenges: UserAuthoredChallenges) {
        hideErrorLayout()
        mainProgress.visibility = View.GONE
        mAuthoredChallengeAdapter.addAllChallenges(userAuthoredChallenges.authoredChallenges)
    }

    private fun showErrorLayout(errorMessage: String?) {
        if (errorLayout.visibility == View.GONE) {
            errorLayout.visibility = View.VISIBLE
            mainProgress.visibility = View.GONE

            if (errorMessage != null)
                errorCauseTextView.text = errorMessage
        }
    }

    private fun hideErrorLayout() {
        if (errorLayout.visibility == View.VISIBLE) {
            errorLayout.visibility = View.GONE
            mainProgress.visibility = View.VISIBLE
        }
    }
}