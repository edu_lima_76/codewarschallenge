package com.eduardolima.codewarschallenge.fragments.authoredchallenges

import com.eduardolima.codewarschallenge.shared.services.user.UserServiceModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(UserServiceModule::class))
interface AuthoredChallengesPresenterComponent {
    fun inject(presenter: AuthoredChallengesPresenter)
}