package com.eduardolima.codewarschallenge.fragments.authoredchallenges

import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AuthoredChallengesPresenterModule::class))
interface AuthoredChallengesFragmentComponent {
    fun inject(fragment: AuthoredChallengesFragment)
}