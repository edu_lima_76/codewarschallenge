package com.eduardolima.codewarschallenge.fragments.completedchallenges

import android.app.Application
import com.eduardolima.codewarschallenge.shared.database.pojo.completedchallenges.UserCompletedChallenges
import com.eduardolima.codewarschallenge.shared.presenter.BasePresenter
import com.eduardolima.codewarschallenge.shared.services.Error
import com.eduardolima.codewarschallenge.shared.services.user.UserService
import com.eduardolima.codewarschallenge.shared.services.user.UserServiceModule
import com.eduardolima.codewarschallenge.shared.views.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import javax.inject.Inject


class CompletedChallengesPresenter(application: Application) : BasePresenter() {

    @Inject
    lateinit var mUserService: UserService

    init {
        DaggerCompletedChallengesPresenterComponent.builder()
                .userServiceModule(UserServiceModule(application))
                .build()
                .inject(this)
    }

    override fun processSuccess(action: Int) {
        if (mView != null) {
            mView!!.onActionSuccess(action)
        }
    }

    override fun processError(action: Int, @Error.Code errorCode: String?) {
        if (mView != null) {
            mView!!.onActionFail(action, errorCode)
        }
    }

    public override fun subscribe(@NonNull view: View) {
        super.subscribe(view)
        mUserService.getStream()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::processEvent)
    }

    public override fun unsubscribe() {
        super.unsubscribe()
    }

    /**
     * Fetch user authored challenges
     */
    fun fetchUserCompletedChallenges(userId: String, page: Int) {
        mUserService.fetchUserCompletedChallenges(userId, page)
    }

    fun getUserCompletedChallenges(): UserCompletedChallenges? {
        return mUserService.getUserCompletedChallenges()
    }
}