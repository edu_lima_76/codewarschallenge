package com.eduardolima.codewarschallenge.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eduardolima.codewarschallenge.R
import com.eduardolima.codewarschallenge.shared.database.pojo.User
import kotlinx.android.synthetic.main.adapter_user_item.view.*


class UserItemAdapter(private var mListOfUsers: MutableList<User>,
                      val clickListener: (User) -> Unit) : RecyclerView.Adapter<UserItemAdapter.ViewHolder>() {

    private lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_user_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mListOfUsers.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentUser = mListOfUsers.get(position)
        holder.bind(currentUser, clickListener)
    }

    fun setUsers(listOfUsers: List<User>) {
        mListOfUsers.clear()
        mListOfUsers.addAll(listOfUsers)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(user: User, clickListener: (User) -> Unit) = with(itemView) {
            userRank.text = "#${user.rank}"
            userIdTextView.text = user.username
            itemView.setOnClickListener { clickListener(user) }
        }

    }
}
