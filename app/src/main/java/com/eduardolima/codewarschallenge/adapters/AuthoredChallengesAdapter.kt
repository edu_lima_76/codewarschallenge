package com.eduardolima.codewarschallenge.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eduardolima.codewarschallenge.R
import com.eduardolima.codewarschallenge.shared.database.pojo.authoredchallenges.AuthoredChallenge
import kotlinx.android.synthetic.main.adapter_authored_challenge_item.view.*


class AuthoredChallengesAdapter(private val mItemClickListener: (AuthoredChallenge) -> Unit)
    : RecyclerView.Adapter<AuthoredChallengesAdapter.ViewHolder>() {

    private lateinit var mContext: Context
    private var mListOfChallenges: MutableList<AuthoredChallenge> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_authored_challenge_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mListOfChallenges.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentChallenge = mListOfChallenges.get(position)
        holder.bind(currentChallenge, mItemClickListener)
    }

    private fun addChallenge(completedChallenge: AuthoredChallenge) {
        mListOfChallenges.add(completedChallenge)
        notifyItemInserted(mListOfChallenges.size - 1)
    }

    fun addAllChallenges(challenges: List<AuthoredChallenge>) {
        for (challenge in challenges) {
            addChallenge(challenge)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(authoredChallenge: AuthoredChallenge, itemClickListener: (AuthoredChallenge) -> Unit) = with(itemView) {
            authoredChallengeName.text = authoredChallenge.name
            itemView.setOnClickListener { itemClickListener(authoredChallenge) }
        }

    }
}
