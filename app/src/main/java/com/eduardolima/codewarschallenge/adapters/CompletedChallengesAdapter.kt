package com.eduardolima.codewarschallenge.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eduardolima.codewarschallenge.R
import com.eduardolima.codewarschallenge.shared.database.pojo.completedchallenges.CompletedChallenge
import com.eduardolima.codewarschallenge.shared.utils.PaginationAdapterCallback
import kotlinx.android.synthetic.main.adapter_completed_challenge_item.view.*
import kotlinx.android.synthetic.main.adapter_item_progress.view.*


class CompletedChallengesAdapter(val mItemClickListener: (CompletedChallenge) -> Unit,
                                 val mContract: PaginationAdapterCallback) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val ITEM = 0
    private val LOADING = 1

    private var mListOfChallenges: MutableList<CompletedChallenge> = ArrayList()
    private var mIsLoadingAdded = false
    private var mRetryPageLoad = false
    private var mErrorMsg: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ITEM -> {
                val viewItem = inflater.inflate(R.layout.adapter_completed_challenge_item, parent, false)
                viewHolder = CompletedChallengeViewHolder(viewItem)
            }
            LOADING -> {
                val viewItem = inflater.inflate(R.layout.adapter_item_progress, parent, false)
                viewHolder = LoadingViewHolder(viewItem)
            }
        }

        return viewHolder!!
    }

    override fun getItemCount(): Int {
        return mListOfChallenges.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mListOfChallenges.size - 1 && mIsLoadingAdded) LOADING else ITEM
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currentChallenge = mListOfChallenges.get(position)

        when (getItemViewType(position)) {
            ITEM -> binItemViewHolder(holder, currentChallenge)
            LOADING -> binLoadingViewHolder(holder)
        }
    }

    private fun binItemViewHolder(holder: RecyclerView.ViewHolder, currentChallenge: CompletedChallenge) {
        val itemViewHolder = holder as CompletedChallengeViewHolder
        itemViewHolder.bindItem(currentChallenge, mItemClickListener)
    }

    private fun binLoadingViewHolder(holder: RecyclerView.ViewHolder) {
        val itemViewHolder = holder as LoadingViewHolder
        itemViewHolder.bindLoading(mContract, mRetryPageLoad, mErrorMsg)
    }

    private fun addChallenge(completedChallenge: CompletedChallenge) {
        mListOfChallenges.add(completedChallenge)
        notifyItemInserted(mListOfChallenges.size - 1)
    }

    fun addAllChallenges(challenges: List<CompletedChallenge>) {
        for (challenge in challenges) {
            addChallenge(challenge)
        }
    }

    /**
     * Add loading view when bottom list is reached
     */
    fun addLoadingFooter() {
        mIsLoadingAdded = true
        addChallenge(CompletedChallenge("", ""))
    }

    /**
     * Remove loading when new data is collected
     */
    fun removeLoadingFooter() {
        mIsLoadingAdded = false
        val position = mListOfChallenges.size - 1
        val challenge = getItem(position)

        if (challenge != null) {
            mListOfChallenges.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    private fun getItem(position: Int): CompletedChallenge {
        return mListOfChallenges.get(position)
    }

    /**
     * Show retry layout, when an error occur
     */
    fun showRetry(show: Boolean, errorMsg: String?) {
        mRetryPageLoad = show
        notifyItemChanged(mListOfChallenges.size - 1)

        if (errorMsg != null) mErrorMsg = errorMsg
    }

    /**
     * VIEW HOLDER's
     */

    /**
     * Challenge view holder
     */
    class CompletedChallengeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItem(completedChallenge: CompletedChallenge,
                     itemClickListener: (CompletedChallenge) -> Unit) = with(itemView) {
            completedChallengeName.text = completedChallenge.name
            itemView.setOnClickListener { itemClickListener(completedChallenge) }
        }

    }

    /**
     * Loading view holder
     */
    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindLoading(mContract: PaginationAdapterCallback, retryPageLoad: Boolean, errorMsg: String?) = with(itemView) {

            if (retryPageLoad) {
                loadmoreErrorContainer.visibility = View.VISIBLE
                loadmoreProgress.visibility = View.GONE

                loadmoreErrorTextView.text =
                        errorMsg ?: context.getString(R.string.error_something_went_wrong)
            } else {
                loadmoreErrorContainer.visibility = View.GONE
                loadmoreProgress.visibility = View.VISIBLE
            }

            loadmoreErrorContainer.setOnClickListener { mContract.retryPageLoad() }
        }

    }
}